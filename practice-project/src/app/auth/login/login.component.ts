/*

 */

import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";
import { Subscription } from "rxjs";
import { AuthService } from "../auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  isLoading = false;
  private isAuthSubs!: Subscription;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.isAuthSubs = this.authService.getAuthStatus().subscribe((res) => {
      this.isLoading = false;
    })
  }

  onLogin(form: NgForm) {
    if(form.invalid) {
      return;
    }
    this.isLoading = true;
    this.authService.loginUser(form.value.email, form.value.pswd);
  }

  ngOnDestroy() {
    this.isAuthSubs.unsubscribe();
  }
}
