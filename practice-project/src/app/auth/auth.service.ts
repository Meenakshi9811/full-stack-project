/*

 */

import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { Subject } from "rxjs";
import { environment } from "src/environments/environment";

const BACKEND_URL = environment.apiUrl + '/user/';
export interface AuthData {
  email: string,
  password: string
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private token: string = "";
  private isAuthenticated = new Subject<boolean>();
  private isAuth = false;
  private tokenTimer: any;
  private userId: string = '';
  constructor(public http: HttpClient, private router: Router) { }

  public getToken() {
    return this.token;
  }

  public getUserId() {
    return this.userId;
  }

  public getAuthStatus() {
    return this.isAuthenticated.asObservable();
  }

  public getisAuth() {
    return this.isAuth;
  }

  createUser(email: string, pswd: string) {
    const user: AuthData = {email: email, password: pswd}
    this.http.post(BACKEND_URL + 'signup', user).subscribe(res => {
      this.router.navigate(['/']);
    }, error => {
      this.isAuthenticated.next(false);
    });
  }

  loginUser(email: string, pswd: string) {
    const user: AuthData = {email: email, password: pswd}
    this.http.post<{token: string, timer: number, userId: string}>(BACKEND_URL + 'login', user).subscribe(res => {
      this.token = res.token;
      this.userId = res.userId;
      if(this.token) {
        const expiresIn = res.timer;
        this,this.setTimeOut(expiresIn);
        this.isAuth = true;
        this.isAuthenticated.next(true);
        const now = new Date();
        const expirydate = new Date(now.getTime() + expiresIn *1000);
        localStorage.setItem('timer', expirydate.toISOString());
        localStorage.setItem('token', this.token);
        localStorage.setItem('userId', this.userId);
        this.router.navigate(['/']);
      }
    }, error => {
      this.isAuthenticated.next(false);
    });
  }

  logout() {
    this.token = 'null';
    this.isAuth = false;
    this.isAuthenticated.next(false);
    clearTimeout(this.tokenTimer);
    this.router.navigate(['/']);
    localStorage.removeItem('token');
    localStorage.removeItem('timer');
    localStorage.removeItem('userId');
    this.userId = '';
  }

  autoAuthenticate() {
    const storedToken = window.localStorage.getItem('token');
    const timer: string = localStorage.getItem('timer') as string;
    this.userId = localStorage.getItem('userId') as string;
    const expirationDate = new Date(timer);
    if(!storedToken || !expirationDate) {
      return;
    }
    const validTime =  expirationDate.getTime() - new Date().getTime();
    if (validTime > 0) {
      this.token = storedToken;
      this.isAuth = true;
      this.isAuthenticated.next(true);
      this.setTimeOut(validTime/1000);
    }

  }

  setTimeOut(expiresIn: number) {
    this.tokenTimer = setTimeout(() => {
      this.logout();
    }, expiresIn * 1000);
  }
}
