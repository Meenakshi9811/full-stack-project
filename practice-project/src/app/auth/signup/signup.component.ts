/*

 */

import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";
import { Subscription } from "rxjs";
import { AuthService } from "../auth.service";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  isLoading = false;
  private isAuthSubs!: Subscription;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.isAuthSubs = this.authService.getAuthStatus().subscribe((res) => {
      this.isLoading = false;
    })
  }

  onSignUp(form: NgForm) {
    if(form.invalid) {
      return;
    }
    this.isLoading = true;
    this.authService.createUser(form.value.email, form.value.pswd);
  }

  ngOnDestroy() {
    this.isAuthSubs.unsubscribe();
  }
}
