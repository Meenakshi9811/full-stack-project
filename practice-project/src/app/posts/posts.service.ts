/*

 */

import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { Subject } from "rxjs";
import { PostType } from "./post-create/post-create.component";
import { map } from "rxjs/operators";
import { Router } from "@angular/router";
import { environment } from "src/environments/environment";

const BACKEND_URL = environment.apiUrl + '/posts/';
@Injectable({
  providedIn: 'root'
})
export class PostsService {
  private posts: PostType[] = [];
  private postsUpdate = new Subject<{post: PostType[], count: number}>();

  getPost(postPerPage: number, currentPage: number) {
    /**
     * return this.posts will return the original array
     * arrays and objects in JS/TS are reference types which is a type
     * where if you copy it, you actually copy the address
     * ... is a spread operator, which creates a copy of the array,
     * so that when the data is changed, it will not impact the original data i.e this.posts
     *
     * return [...this.posts];
     */
    //
    const queryParams = `?pageSize=${postPerPage}&page=${currentPage}`;
    this.http
    .get<{message: string, post: any; maxPosts: number}>(
      BACKEND_URL + queryParams
      ) //passing the received data through pipe to transform it into required format before subscribing
      //here we are passsing thereceived data through a pipe to get response _id as id and the return is
      // again an observable which can be subscribed in the next line, but now the return is only the posts
      .pipe(map((res) => {
        return {
          post: res.post.map((post: any) => {
            return {
              title: post.title,
              content: post.content,
              id: post._id,
              imagePath: post.imagePath,
              creator: post.creator
            };
          }),
        maxPosts: res.maxPosts
      };
      }))
    .subscribe((transformedPosts) => {
      this.posts = transformedPosts.post;
      this.postsUpdate.next({post: [...this.posts], count: transformedPosts.maxPosts});
    })
  }

  /**
   *
   * we don't pass directly this.postUpdate because it's a kind of emitter which other would be able to use,
   * therefore we only pass it as an observable.
   */
  getPostUpdate() {
    return this.postsUpdate.asObservable();
  }

  addPosts(title: string, content: string, image: File) {
    //form data is a data format which allows to combine text and blob
    const postdata = new FormData();
    postdata.append('title', title);
    postdata.append('content', content);
    postdata.append('image', image, title );
    this.http.post<{message: string, post: PostType}>(BACKEND_URL, postdata).subscribe((res)=> {
      console.log(res)
      this.router.navigate(["/"]);
    });
  }

  getPostToEdit(postId: string) {
    return this.http.get<{_id: string, title: string, content: string, imagePath: string, creator: string}>(BACKEND_URL + postId);
  }

  updatePost(postId: string, postTitle: string, postContent: string, image: File | string) {
    let postdata: PostType | FormData;
    if(typeof(image) === 'object') {
      postdata = new FormData();
      postdata.append('id', postId);
      postdata.append('title', postTitle);
      postdata.append('content', postContent);
      postdata.append('image', image, postTitle );
    } else {
      postdata = {id: postId, title: postTitle, content: postContent, imagePath: image, creator: ''}
    }
    this.http.put(BACKEND_URL + postId, postdata).subscribe((res) => {
      this.router.navigate(["/"]);
    })
  }
  deletePost(postID: string) {
    return this.http.delete(BACKEND_URL + postID);
  }

  constructor(private http: HttpClient, private router: Router) { }
}
