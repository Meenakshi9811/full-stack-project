/*

 */

import { Component, Input, OnInit } from '@angular/core';
import { PageEvent } from "@angular/material/paginator";
import { Subscription } from "rxjs";
import { AuthService } from "src/app/auth/auth.service";
import { PostType } from "../post-create/post-create.component";
import { PostsService } from "../posts.service";

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {

  posts: PostType[] =[] ;
  private postSub!: Subscription;
  authSubs!: Subscription;
  userAuthenticated = false;
  isLoading = false;
  totalPosts = 1000;
  postPerPage = 5;
  pageSizeOption = [5, 10, 25, 100];
  currentPage = 1;
  userId: string = '';

  constructor(public postService: PostsService, private authService: AuthService) { }

  ngOnInit(): void {
    /**
     * with just calling it on ngOnInit will not display data because this is called on initialisation
     * when there was no data present.
     * To solve this:
     * 1. instead of passing a copy in this.postService.getPost(), we can pass the refernce typed array
     * i.e instead of ...this.posts, we pass this.post, which would of course has the drawback of changing
     * the original content which we don't want.
     * 2. use event emitter but unfortunately it works with output tag
     * 2. Use observable 'subject' from rxJs which has broader usage than eventEmitter. R
     * RxJs is an external dependency which is useful for passing data around.
     */
     this.isLoading = true;
     this.postService.getPost(this.postPerPage, this.currentPage);
     this.userId = this.authService.getUserId();
     /**
     * subscribe takes 3 params;
     * 1. next(): is a function which is called when there is a new value
     * - here we have used an unmaed function with param posts
     * 2. error(): is a function which is called when there is an error
     * 3. complete(): also a function called when there can be no more inputs,
     * basically no more next() calls
     */
    this.postSub = this.postService.getPostUpdate().subscribe((postData: any)=> {
      this.isLoading = false;
      this.totalPosts = postData.maxCounts;
      this.posts = postData.post;
    });
    this.userAuthenticated = this.authService.getisAuth();
    this.authSubs = this.authService.getAuthStatus().subscribe(isAuthenticated => {
      this.userAuthenticated = isAuthenticated;
      this.userId = this.authService.getUserId();
    });
  }

  onDelete(postID: string) {
    this.isLoading = true;
    this.postService.deletePost(postID).subscribe(() => {
      this.postService.getPost(this.postPerPage, this.currentPage);
    });
  }
  /**
   * when this components is closed, it is important to unsubscribe to the subscription
   * above to prevent memory leak. For this we need to store it in postSub of type Subscription
   * and unsubscribe to it on ngOnDestroy
   */

  onPageChange(pageData: PageEvent) {
    this.isLoading = true;
    this.currentPage = pageData.pageIndex + 1;
    this.postPerPage = pageData.pageSize
    this.postService.getPost(this.postPerPage, this.currentPage);
  }

  ngOnDestroy() {
    this.postSub.unsubscribe();
    this.authSubs.unsubscribe();
  }
}
