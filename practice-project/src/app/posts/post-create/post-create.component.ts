/*

 */

import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, ParamMap } from "@angular/router";
import { PostsService } from "../posts.service";
import { mimeType } from "./mime-type.validator";

export interface PostType {
  id: string,
  title: string,
  content: string,
  imagePath: string,
  creator: string
}

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.css']
})
export class PostCreateComponent implements OnInit {

  mode = "create";
  postId!: any;
  post: PostType = {id: '', title: '', content: '', imagePath: '', creator: ''};
  isLoading = false;
  form!: FormGroup;
  imagePreview!: string;

  constructor(public postService: PostsService, private route: ActivatedRoute) {}
  ngOnInit(): void {
    this.form = new FormGroup({
      'title': new FormControl(null, {validators: [Validators.required, Validators.minLength(3)]}),
      'content': new FormControl(null, {validators: [Validators.required, Validators.minLength(3)]}),
      'image': new FormControl(null, {validators: [Validators.required], asyncValidators: [mimeType]})
    });
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if(paramMap.has('postId')) {
        this.mode = 'edit';
        this.postId = paramMap.get('postId');
        this.isLoading = true;
        this.postService.getPostToEdit(this.postId).subscribe(result => {
          this.isLoading = false;
          this.post = {id: result._id, title: result.title, content: result.content, imagePath: result.imagePath, creator: result.creator};
          this.form.setValue({
            'title': this.post.title,
            'content': this.post.content,
            'image': this.post.imagePath
          });
        });
      }
      else {
        this.mode = 'create';
        this.postId = null;
      }
    });
  }

  onAddPost() {
    if(this.form.invalid) {
      return;
    }
    this.isLoading = true;
    if(this.mode == 'create') {
      this.postService.addPosts(this.form.value.title, this.form.value.content, this.form.value.image);
      console.log('post create')
    }
    else {
      this.postService.updatePost(this.postId, this.form.value.title, this.form.value.content, this.form.value.image);
    }
    this.form.reset();
  }

  onImagePicked(event: Event) {
    const file = (event.target as HTMLInputElement).files;
    this.form.patchValue({image: file![0]});
    this.form.get('image')?.updateValueAndValidity();
    const reader = new FileReader();
    // this is an asynchronous fn that's why we have used it like this:
    reader.onload = () => {
      this.imagePreview = reader.result as string;
    };
    reader.readAsDataURL(file![0]);

  }

}
