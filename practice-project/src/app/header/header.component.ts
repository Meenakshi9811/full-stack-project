/*

 */

import { Component, OnInit } from '@angular/core';
import { Subscription } from "rxjs";
import { AuthService } from "../auth/auth.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  authSubs!: Subscription;
  userAuthenticated = false;
  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.userAuthenticated = this.authService.getisAuth();
    this.authSubs = this.authService.getAuthStatus().subscribe(isAuthenticated => {
      this.userAuthenticated = isAuthenticated;
      console.log(isAuthenticated)
    });
  }

  ngOnDestroy() {
    this.authSubs.unsubscribe();
  }

  logout() {
    this.authService.logout();
  }

}
