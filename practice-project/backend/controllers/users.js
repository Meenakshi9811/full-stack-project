const User = require('../models/user');
const bcrpt = require('bcrypt');
const jwt = require('jsonwebtoken');

exports.createUser = (req, res, next) => {
  bcrpt.hash(req.body.password, 10).then(hash => {
    const user = new User({
      email: req.body.email,
      password: hash
    });
    user.save().then(result => {
      res.status(201).json({
        message: 'User saved',
        result: result
      });
    })
    .catch(err => {
      res.status(500).json({
        error: {
          message: 'Invalid Credentials'
        }
      });
    });
  });
}

exports.loginUser =(req, res, next) => {
  let fetchedUser;
  User.findOne({email: req.body.email}).then((user => {
    if(!user) {
      return res.status(401).json({
        message: 'User does not exist'
      });
    }
    fetchedUser = user;
    bcrpt.compare(req.body.password, user.password).then(result => {
      if(!result) {
        return res.status(401).json({
          message: 'Incorrect Password'
        });
      }
      const token = jwt.sign({email: fetchedUser.email, userId: fetchedUser._id}, 'secret_key_Should_be_long', {expiresIn: '1h'});
      res.status(200).json({
        token: token,
        timer: 36000,
        userId: fetchedUser._id
      });
    }).catch(err => {
      return res.status(401).json({
        error: {
          message: 'Invalid Credentials'
        }
      });
    });
  }));
}

exports.get = (req, res, next) => {
  const pageSize = +req.query.pageSize;
  const currentPage = +req.query.page;
  const postQuery = Post.find();
  let fetchedPosts;
  if(pageSize && currentPage) {
    postQuery.skip(pageSize * (currentPage - 1))
    .limit(pageSize);
  }
  // using mongoose model to fetch data from the db
  postQuery
    .then(documents => {
      fetchedPosts = documents;
      return Post.countDocuments();
    })
    .then(count => {
      // res.json(posts); //sends post data as json to the website requesting
      // or you can send whatever you want with the data
      // set status to 200 to mention that the website as successfully reached here
      res.status(200).json( {
        message: 'Posts fetched successfully',
        post: fetchedPosts,
        maxPosts: count
      });
    })
    .catch(error => {
      res.status(500).json({
        message:'Could not fetch post'
      })
    });
  }
