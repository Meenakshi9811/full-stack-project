const express = require('express');
const multer = require('multer');
const Post = require('../models/posts');

exports.post =  (req, res, next) => {
  const url = req.protocol + "://" + req.get('host');
  //using mongoose data model
  const post = new Post({
    title: req.body.title,
    content: req.body.content,
    imagePath: url + "/images/" + req.file.filename,
    creator: req.userId
  });
  // just typing the next line will save the data in our mongodb database
  // this is possible because of mongoose, which creates these methods for data created with its model
  post.save().then(result => {
    res.status(201).json({
      message: 'Post added successfully',
      post: {
        ...result,
        id: result._id
      }
    });
  })
  .catch(error => {
    req.stale(500).json({
      message: 'Post creation failed'
    })
  });
}

exports.get = (req, res, next) => {
  const pageSize = +req.query.pageSize;
  const currentPage = +req.query.page;
  const postQuery = Post.find();
  let fetchedPosts;
  if(pageSize && currentPage) {
    postQuery.skip(pageSize * (currentPage - 1))
    .limit(pageSize);
  }
  // using mongoose model to fetch data from the db
  postQuery
    .then(documents => {
      fetchedPosts = documents;
      return Post.countDocuments();
    })
    .then(count => {
      // res.json(posts); //sends post data as json to the website requesting
      // or you can send whatever you want with the data
      // set status to 200 to mention that the website as successfully reached here
      res.status(200).json( {
        message: 'Posts fetched successfully',
        post: fetchedPosts,
        maxPosts: count
      });
    })
    .catch(error => {
      res.status(500).json({
        message:'Could not fetch post'
      })
    }
  );
}

exports.delete =  (req, res, next) => {
  Post.deleteOne({_id: req.params.id, creator: req.userId}).then(result => {
    if(result.n > 0) {
      res.status(200).json({
        message: 'Deleted'
      })
    } else {
      res.status(401).json({
        message:  'Unauthorised to delete'
      })
    }
  }).catch(error => {
    res.status(500).json({
      message:'Could not fetch post'
    })
  })
}

exports.modify = (req, res, next) => {
  let imagePath = req.body.imagePath;
    if (req.file) {
      const url = req.protocol + "://" + req.get("host");
      imagePath = url + "/images/" + req.file.filename
    }
    const post = new Post({
      _id: req.body.id,
      title: req.body.title,
      content: req.body.content,
      imagePath: imagePath,
      creator: req.userId
    });
    Post.updateOne({ _id: req.params.id, creator: req.userId }, post).then(result => {
      if(result.n > 0) {
        res.status(200).json({ message: "Update successful!" });
      } else {
        res.status(400).json({ message: "Unauthorized to modify this post" });
      }
    })
    .catch (error => {
      res.status(500).json({
        message: 'Could not update post'
      })
    }
  );
}

exports.findOne = (req, res, next) => {
  Post.findById(req.params.id).then(post => {
    if(post){
      res.status(200).json(post)
    } else {
      res.status(404).json({message: 'Post not found'})
    }
  }).catch(error => {
    res.status(500).json({
      message:'Could not fetch post'
    })
  })
}
