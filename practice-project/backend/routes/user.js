const express = require('express');
const router = express.Router();
const User = require('../models/user');
const bcrpt = require('bcrypt');
const jwt = require('jsonwebtoken');
const userController = require('../controllers/users');

router.post('/signup', userController.createUser);
router.post('/login', userController.loginUser);

module.exports = router;
