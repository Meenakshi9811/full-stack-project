const express = require('express');
const multer = require('multer');
const router = express.Router();
const Post = require('../models/posts');
const checkAuth = require('../middleware/check-auth');
const postController = require('../controllers/posts');
const extractFile = require('../middleware/file');

router.post('', checkAuth, extractFile, postController.post);

//this first argument i.e path tells that requests directed to this path will be handled
router.get('', postController.get);

router.delete('/:id', checkAuth, postController.delete);

router.put('/:id', checkAuth, extractFile, postController.modify);

router.get('/:id', postController.findOne)

module.exports = router;
