const jwt = require('jsonwebtoken');


module.exports = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1];
    creatorId = jwt.verify(token, 'secret_key_Should_be_long');
    req.userId = creatorId.userId;
    next();
  } catch (err){
    res.status(401).json({
      error: err
    })
  }
}
