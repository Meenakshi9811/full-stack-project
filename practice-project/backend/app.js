const express = require('express'); // framework
const bodyParser = require('body-parser');
const path = require('path');
const mongooose = require('mongoose');
const postsRoute = require('./routes/posts');
const userRoute = require('./routes/user');
// create an app and store it as an constant so it can't be changed

const app = express(); //this returns an app which can be used
/**
 * An express app is basically a big chain of middleware we apply to incoming requests
 * kind of funnel through which we pass our response and every part of the funnel do something
 * to our response for example modify it or send the response.
 *  add such a middleware here which would be applied to each req.

app.use((req, res, next) => {
  console.log('first middleware');
  next(); //tells it to continue to next middleware
});
app.use((req, res, next) => {
  res.send('hello from app'); //send response to the server
});
1o2wamd7Cr1IwcgK
 */

mongooose.connect('mongodb+srv://meenakshi:1o2wamd7Cr1IwcgK@test.np2eo.mongodb.net/myFirstDatabase?retryWrites=true&w=majority')
  .then(() => {
    console.log('connected to database');
  })
  .catch(() => {
    console.log('connection to databasse failed');
  })
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use('/images', express.static(path.join('backend/images')));

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*'),
  res.setHeader('Access-Control-Allow-Headers', '*'),
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, DELETE, OPTIONS')
  next();
})

app.use('/api/posts', postsRoute);
app.use('/api/user', userRoute);
//export this app
module.exports = app;
